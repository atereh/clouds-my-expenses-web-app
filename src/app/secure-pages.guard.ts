import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ExpensesService } from './expenses.service';

@Injectable({
  providedIn: 'root'
})
export class SecurePagesGuard implements CanActivate {

  constructor(private expensesService: ExpensesService, private router : Router){};
  
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    //check if user can access a certain layout
      if(this.expensesService.userSignedIn()){ //check if user signed in
       
      this.router.navigate(["expenses"]); //navigate to expenses page
      }
      return true;
  }
  
}
