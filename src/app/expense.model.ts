export class Expense{
    date?: any;
    description?: string;
    amount?: number;

    constructor(date: any,
        description: string,
        amount: number) {
            this.date = date;
            this.description = description;
            this.amount = amount;
        }
}