import { Injectable } from '@angular/core';
import  firebase  from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore} from '@angular/fire/firestore';
import { User } from './user';
import { Router } from '@angular/router';
import { Expense } from './expense.model';

@Injectable({
  providedIn: 'root'
})
export class ExpensesService {

  private user!: User;

  constructor(private afAuth: AngularFireAuth, private router: Router,
    private firestore: AngularFirestore) { }

  async signInWithGoogle(){
    const credentials = await this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    this.user = {
      uid: credentials.user?.uid,
      displayName: credentials.user?.displayName,
      email: credentials.user?.email
    };
    localStorage.setItem("user", JSON.stringify(this.user)); //saving the user information in local storage in a variable called user
    this.updateUserData();
    this.router.navigate(["expenses"]);


  }

  private updateUserData(){
    this.firestore.collection("users").doc(this.user.uid).set({

      uid: this.user.uid,
      displayName: this.user.displayName,
      email: this.user.email

    }, {merge: true});

  }

  getUser(){
    if (this.userSignedIn()){
      this.user = JSON.parse(localStorage.getItem("user")!);
    }
    return this.user;
  }

  userSignedIn() : boolean {
    return JSON.parse(localStorage.getItem("user")!) != null;
  }

  signOut() {
    this.afAuth.signOut(); //we first call the sign out method of the angular firestore auth
    localStorage.removeItem("user"); //remove user from local store
    //this.user = null; //we set the service user variable to null.
    this.router.navigate(["signin"]); //go back to signIn page

  }

  getExpenses(){
    //From the firestore users collection, 
    //we reach the user document corresponding to the user who is signed in. 
    //From there, we reach the collection of expenses corresponding to this user.

    return this.firestore.collection("users")
    .doc(this.user.uid).collection("expenses").valueChanges(); //We apply an order by filter on the reference of the dataset to order it by date ascendently.
    //We return the “valueChanges” observable that will keep the retrieved expenses dataset up-to-date.
  }

  addExpense(expense: Expense){
    //This method will add an expense document in the collection of expenses 
    //belonging to the signed in user, in the collection of firestore users.
    this.firestore.collection("users").doc(this.user.uid)
    .collection("expenses").add(expense);

  

  }
}
