import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ExpensesService } from './expenses.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private expensesService: ExpensesService, private router : Router){};

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    //check if user can access a certain layout
      if(!this.expensesService.userSignedIn()){ //check if not signed in
        this.router.navigate(["signin"]); //navigate to sign in page
      }
    return true;
  }
  
}
