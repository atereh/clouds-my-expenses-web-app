// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyC_cHeHTHHf0SkfpgxV-SXLyNgXOIr0_Dw",
    authDomain: "expenses-83002.firebaseapp.com",
    projectId: "expenses-83002",
    storageBucket: "expenses-83002.appspot.com",
    messagingSenderId: "403195565634",
    appId: "1:403195565634:web:a3ac8548024780d8bebfdb",
    measurementId: "G-QB6LWMDZKB"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
